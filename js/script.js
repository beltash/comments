$( document ).ready(function() {
    drawInputField("root", '0');
});
var max_depth = 6;

//отрисовка формы для отправки комментария
function drawInputField(id, depth_level){
	var depth = parseInt(depth_level)+1;
	$(".form-append:not(#form_root)").remove();
	
	$('#id_'+id).append('<form id="form_'+id+'" class="form-append">' +
				'<div class="input-container">'+
				'<span  class="text-comment">Ваше имя</span><br>' +
				'<input name="author" style="border: 1px solid #888;" type="text"></input><br>' +
			'</div>' +
			'<div class="input-container">' +
				'<span  class="text-comment">Комментарий</span><br>' +
				'<textarea name="text" style="border: 1px solid #888;" rows="4" cols="45" name="text"></textarea><br>' +
			'</div>' +
			'<input type="hidden" name="depth_level" value="'+depth+'">' +
			'<input type="hidden" name="parent_id" value="'+id+'">' +
			'<div class="input-container">' + 
				'<span class="button" onclick="addComment(\'form_'+id+'\')">Отправить</span>' +
			'</div>' +
		'</form>'
	);
}

//удаление комментария (удаление потомков реализовано на уровне БД)
function deleteNode(id){
	$.ajax({
		type: 'POST',
		url: 'deleteComment.php',
		data: {'id': id},
		success: function(data) {
			if(data)
				$('#id_'+id).remove();
			console.log(data);
		},
		error:  function(xhr, str){
			alert('Возникла ошибка: ' + xhr.responseCode);
		}
	});
}

//добавление комментария
function addComment(form_id){
	var msg   = $('#'+form_id).serialize();
	$.ajax({
		type: 'POST',
		url: 'addComment.php',
		data: msg,
		success: function(data) {
			var obj = jQuery.parseJSON(data);
			var author = $("#"+form_id + " input[name='author']").val(); 
			var text = $("#"+form_id + " [name='text']").val();
			var depth_level = $("#"+form_id + " [name='depth_level']").val();
			var parent_id = 'root';
			if(obj.parent_id!=0)
				var parent_id = obj.parent_id;
				
			if(depth_level< max_depth) 
				$('#'+form_id).before('<div id="id_'+obj.id+'" class="depth_'+depth_level+' comment-container">\
					<span class="comment-info author">'+author+'</span><span class="comment-info date">только что</span>\
					<span class="comment-info date response" onclick="drawInputField('+obj.id+', '+depth_level+')">Ответить</span>\
					<span class="comment-info date response" onclick="deleteNode('+obj.id+')">Удалить</span><br>\
					<span class="text-comment">'+text+'</span>\
					</div>');
			else
				$('#'+form_id).before('<div id="id_'+obj.id+'" class="depth_'+depth_level+' comment-container">\
					<span class="comment-info author">'+author+'</span><span class="comment-info date">только что</span>\
					<span class="comment-info date response" onclick="deleteNode('+obj.id+')">Удалить</span><br>\
					<span class="text-comment">'+text+'</span>\
					</div>');	
			if(depth_level==1)
				$('#'+form_id)[0].reset();
			else
				$('#'+form_id).remove();
			console.log(data);
		},
		error:  function(xhr, str){
			alert('Возникла ошибка: ' + xhr.responseCode);
		}
	});
}