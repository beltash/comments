<link rel="stylesheet" type="text/css" href="/css/style.css">
<script src="/js/jquery.min.js"></script>
<script src="/js/script.js"></script>
<div id="id_root">
<?php
require('connectDB.php');
$MAX_DEPTH = 6;
$arChildren = array();

if($result = $mysqli->query ('SELECT * FROM comments')){
	while ($row = $result->fetch_assoc()) {
		if($row['depth_level']!=1)
			$arChildren[$row['parent_id']][] = $row;
		else
			$arChildren['root'][] = $row;
    }

	foreach($arChildren['root'] as $key=>$value){
		echo getHtml($value, '', $arChildren, $MAX_DEPTH);
	}
    $result->free();
}
?>
</div>
<?php

// формирование вложенных комментариев
function getHtml($currentComment, $str, $arChildren, $MAX_DEPTH){
	$depth = $currentComment['depth_level'];
	if($depth<$MAX_DEPTH)
	$str .= '<div id="id_'.$currentComment['id'].'" class="depth_'.$currentComment['depth_level'].' comment-container">
		<span class="comment-info author">'.$currentComment['author'].'</span>
		<span class="comment-info date">'.$currentComment['date'].'</span>
		<span class="comment-info date response" onclick="drawInputField('.$currentComment['id'].', '.$depth.')">Ответить</span>
		<span class="comment-info date response" onclick="deleteNode('.$currentComment['id'].')">Удалить</span><br>
		<span class="text-comment">'.$currentComment['text'].'</span>';
	else
		$str .= '<div id="id_'.$currentComment['id'].'" class="depth_'.$currentComment['depth_level'].' comment-container">
		<span class="comment-info author">'.$currentComment['author'].'</span>
		<span class="comment-info date">'.$currentComment['date'].'</span>
		<span class="comment-info date response" onclick="deleteNode('.$currentComment['id'].')">Удалить</span><br>
		<span class="text-comment">'.$currentComment['text'].'</span>';
 
	if(array_key_exists($currentComment['id'], $arChildren)){
		foreach($arChildren[$currentComment['id']] as $key=>$value){
			$str = getHtml($value, $str, $arChildren, $MAX_DEPTH);
		}
	}
	$str .='</div>';
	return $str;
}
?>