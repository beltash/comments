<?php
require('connectDB.php');
if($_POST['parent_id']=='root'){
	$stmt = $mysqli->prepare("INSERT INTO `comments` (`text`, `depth_level`, `author`)  VALUES (?, ?, ?)");
	$stmt->bind_param('sis', $text, $depth, $author);
	
	$text = $_POST['text'] ;
	$depth = (int)$_POST['depth_level'];
	$author = $_POST['author'] ;
	
	$stmt->execute();
}
else{
	
	$stmt = $mysqli->prepare("INSERT INTO `comments` (`text`, `depth_level`, `author`, `parent_id`) VALUES (?, ?, ?, ?)");
	$stmt->bind_param('sisi', $text, $depth, $author, $parent);
	
	$text = $_POST['text'] ;
	$depth = (int)($_POST['depth_level']) ;
	$author = $_POST['author'] ;
	$parent = (int)$_POST['parent_id'] ;
	
	$stmt->execute();
}
echo '{"id": '.$stmt->insert_id.' , "parent_id":"'.$_POST['parent_id'].'"}';
?>
